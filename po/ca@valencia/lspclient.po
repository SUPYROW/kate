# Translation of lspclient.po to Catalan (Valencian)
# Copyright (C) 2019-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2019, 2020, 2021, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-13 00:49+0000\n"
"PO-Revision-Date: 2022-11-13 09:30+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:250
#, kde-format
msgid "Filter..."
msgstr "Filtre..."

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:101 lspclientconfigpage.cpp:106
#: lspclientpluginview.cpp:596 lspclientpluginview.cpp:733 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "Client LSP"

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "No JSON data to validate."
msgstr "No hi ha dades JSON que s'hagen de validar."

#: lspclientconfigpage.cpp:233
#, kde-format
msgid "JSON data is valid."
msgstr "Les dades JSON són vàlides."

#: lspclientconfigpage.cpp:235
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "Les dades JSON no són vàlides: no és un objecte JSON"

#: lspclientconfigpage.cpp:238
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "Les dades JSON no són vàlides: %1"

#: lspclientconfigpage.cpp:286
#, kde-format
msgid "Delete selected entries"
msgstr "Elimina les entrades seleccionades"

#: lspclientconfigpage.cpp:291
#, kde-format
msgid "Delete all entries"
msgstr "Suprimix totes les entrades"

#: lspclientplugin.cpp:228
#, kde-format
msgid "LSP server start requested"
msgstr "Sol·licitud d'inici del servidor LSP"

#: lspclientplugin.cpp:231
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""
"Voleu iniciar el servidor LSP?<br><br>La línia d'ordres sencera és:"
"<br><br><b>%1</b><br><br>L'opció es pot alterar a través de la pàgina de "
"configuració del connector."

#: lspclientplugin.cpp:244
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""
"L'usuari ha bloquejat permanentment l'inici de: «%1».\n"
"Utilitzeu la pàgina de configuració del connector per a desfer este bloqueig."

#: lspclientpluginview.cpp:608
#, kde-format
msgid "Go to Definition"
msgstr "Ves a la definició"

#: lspclientpluginview.cpp:610
#, kde-format
msgid "Go to Declaration"
msgstr "Ves a la declaració"

#: lspclientpluginview.cpp:612
#, kde-format
msgid "Go to Type Definition"
msgstr "Ves a la definició del tipus"

#: lspclientpluginview.cpp:614
#, kde-format
msgid "Find References"
msgstr "Troba les referències"

#: lspclientpluginview.cpp:617
#, kde-format
msgid "Find Implementations"
msgstr "Troba les implementacions"

#: lspclientpluginview.cpp:619
#, kde-format
msgid "Highlight"
msgstr "Ressaltat"

#: lspclientpluginview.cpp:621
#, kde-format
msgid "Symbol Info"
msgstr "Informació dels símbols"

#: lspclientpluginview.cpp:623
#, kde-format
msgid "Search and Go to Symbol"
msgstr "Busca i ves al símbol"

#: lspclientpluginview.cpp:628
#, kde-format
msgid "Format"
msgstr "Format"

#: lspclientpluginview.cpp:631
#, kde-format
msgid "Rename"
msgstr "Canvia el nom"

#: lspclientpluginview.cpp:635
#, kde-format
msgid "Expand Selection"
msgstr "Expandix la selecció"

#: lspclientpluginview.cpp:638
#, kde-format
msgid "Shrink Selection"
msgstr "Fes encollir la selecció"

#: lspclientpluginview.cpp:642
#, kde-format
msgid "Switch Source Header"
msgstr "Canvia la capçalera del codi font"

#: lspclientpluginview.cpp:645
#, kde-format
msgid "Expand Macro"
msgstr "Expandix la macro"

#: lspclientpluginview.cpp:647
#, kde-format
msgid "Quick Fix"
msgstr "Esmena ràpida"

#: lspclientpluginview.cpp:650
#, kde-format
msgid "Code Action"
msgstr "Acció del codi"

#: lspclientpluginview.cpp:656
#, kde-format
msgid "Show selected completion documentation"
msgstr "Mostra la documentació de compleció seleccionada"

#: lspclientpluginview.cpp:659
#, kde-format
msgid "Enable signature help with auto completion"
msgstr "Activa l'ajuda de signatures amb la compleció automàtica"

#: lspclientpluginview.cpp:662
#, kde-format
msgid "Include declaration in references"
msgstr "Inclou la declaració a les referències"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:665 lspconfigwidget.ui:68
#, kde-format
msgid "Add parentheses upon function completion"
msgstr "Afig un parèntesi a la finalització de la funció"

#: lspclientpluginview.cpp:668
#, kde-format
msgid "Show hover information"
msgstr "Mostra la informació en passar per damunt"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:671 lspconfigwidget.ui:234
#, kde-format
msgid "Format on typing"
msgstr "Format per a escriure"

#: lspclientpluginview.cpp:674
#, kde-format
msgid "Incremental document synchronization"
msgstr "Sincronització incremental de la documentació"

#: lspclientpluginview.cpp:677
#, kde-format
msgid "Highlight goto location"
msgstr "Ressalta la ubicació «goto»"

#: lspclientpluginview.cpp:682
#, kde-format
msgid "Show Diagnostics Notifications"
msgstr "Mostra les notificacions de diagnòstic"

#: lspclientpluginview.cpp:685
#, kde-format
msgid "Show Diagnostics Highlights"
msgstr "Mostra els ressaltats de diagnòstic"

#: lspclientpluginview.cpp:688
#, kde-format
msgid "Show Diagnostics Marks"
msgstr "Mostra les marques de diagnòstic"

#: lspclientpluginview.cpp:691
#, kde-format
msgid "Show Diagnostics on Hover"
msgstr "Mostra el diagnòstic en passar-hi per damunt"

#: lspclientpluginview.cpp:694
#, kde-format
msgid "Switch to Diagnostics Tab"
msgstr "Canvia a la pestanya de diagnòstic"

#: lspclientpluginview.cpp:698
#, kde-format
msgid "Show Messages"
msgstr "Mostra els missatges"

#: lspclientpluginview.cpp:703
#, kde-format
msgid "Server Memory Usage"
msgstr "Ús de la memòria del servidor"

#: lspclientpluginview.cpp:707
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr "Tanca totes les pestanyes de referència dinàmica"

#: lspclientpluginview.cpp:709
#, kde-format
msgid "Restart LSP Server"
msgstr "Reinicia el servidor LSP"

#: lspclientpluginview.cpp:711
#, kde-format
msgid "Restart All LSP Servers"
msgstr "Reinicia tots els servidors LSP"

#: lspclientpluginview.cpp:722
#, kde-format
msgid "Go To"
msgstr "Ves a"

#: lspclientpluginview.cpp:750
#, kde-format
msgid "More options"
msgstr "Més opcions"

#: lspclientpluginview.cpp:782
#, kde-format
msgid "LSP"
msgstr "LSP"

#: lspclientpluginview.cpp:978 lspclientpluginview.cpp:2527
#: lspclientsymbolview.cpp:285
#, kde-format
msgid "Expand All"
msgstr "Expandix-ho tot"

#: lspclientpluginview.cpp:979 lspclientpluginview.cpp:2528
#: lspclientsymbolview.cpp:286
#, kde-format
msgid "Collapse All"
msgstr "Reduïx-ho tot"

#: lspclientpluginview.cpp:1001
#, kde-format
msgctxt "@title:tab"
msgid "Diagnostics"
msgstr "Diagnòstics"

#: lspclientpluginview.cpp:1253
#, kde-format
msgid "RangeHighLight"
msgstr "Interval de ressaltat"

#: lspclientpluginview.cpp:1259
#, kde-format
msgid "Error"
msgstr "S'ha produït un error"

#: lspclientpluginview.cpp:1263
#, kde-format
msgid "Warning"
msgstr "Avís"

#: lspclientpluginview.cpp:1267
#, kde-format
msgid "Information"
msgstr "Informació"

#: lspclientpluginview.cpp:1451
#, kde-format
msgctxt "@info"
msgid ""
"Error in regular expression: %1\n"
"offset %2: %3"
msgstr ""
"S'ha produït un error en l'expressió regular: %1\n"
"desplaçament %2: %3"

#: lspclientpluginview.cpp:1778
#, kde-format
msgid "Line: %1: "
msgstr "Línia: %1: "

#: lspclientpluginview.cpp:1925 lspclientpluginview.cpp:2277
#: lspclientpluginview.cpp:2398
#, kde-format
msgid "No results"
msgstr "Sense resultats"

#: lspclientpluginview.cpp:1984
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "Definició: %1"

#: lspclientpluginview.cpp:1990
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "Declaració: %1"

#: lspclientpluginview.cpp:1996
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "Definició del tipus: %1"

#: lspclientpluginview.cpp:2002
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "Referències: %1"

#: lspclientpluginview.cpp:2014
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "Implementació: %1"

#: lspclientpluginview.cpp:2027
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "Ressaltat: %1"

#: lspclientpluginview.cpp:2051 lspclientpluginview.cpp:2063
#: lspclientpluginview.cpp:2076
#, kde-format
msgid "No Actions"
msgstr "Sense accions"

#: lspclientpluginview.cpp:2067
#, kde-format
msgid "Loading..."
msgstr "S'està carregant..."

#: lspclientpluginview.cpp:2159
#, kde-format
msgid "No edits"
msgstr "Sense edicions"

#: lspclientpluginview.cpp:2235
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "Canvia el nom"

#: lspclientpluginview.cpp:2236
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr "Nom nou (atenció: no es poden substituir totes les referències)"

#: lspclientpluginview.cpp:2283
#, kde-format
msgid "Not enough results"
msgstr "No hi ha prou resultats"

#: lspclientpluginview.cpp:2353
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr "No s'ha trobat la capçalera/codi font corresponent"

#: lspclientpluginview.cpp:2537
#, kde-format
msgid "Copy to Clipboard"
msgstr "Copia al porta-retalls"

#: lspclientpluginview.cpp:2560
#, kde-format
msgid "Remove Global Suppression"
msgstr "Elimina la supressió global"

#: lspclientpluginview.cpp:2562
#, kde-format
msgid "Add Global Suppression"
msgstr "Afig una supressió global"

#: lspclientpluginview.cpp:2566
#, kde-format
msgid "Remove Local Suppression"
msgstr "Elimina la supressió local"

#: lspclientpluginview.cpp:2568
#, kde-format
msgid "Add Local Suppression"
msgstr "Afig una supressió local"

#: lspclientpluginview.cpp:2580
#, kde-format
msgid "Disable Suppression"
msgstr "Desactiva la supressió"

#: lspclientpluginview.cpp:2582
#, kde-format
msgid "Enable Suppression"
msgstr "Activa la supressió"

#: lspclientpluginview.cpp:2728
#, kde-format
msgctxt "@info"
msgid "%1 [suppressed: %2]"
msgstr "%1 [suprimida: %2]"

#: lspclientpluginview.cpp:2843 lspclientpluginview.cpp:2880
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr "Servidor LSP"

#: lspclientpluginview.cpp:2903
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr "Client LSP"

#: lspclientservermanager.cpp:576
#, kde-format
msgid "Restarting"
msgstr "S'està reiniciant"

#: lspclientservermanager.cpp:576
#, kde-format
msgid "NOT Restarting"
msgstr "NO s'està reiniciant"

#: lspclientservermanager.cpp:577
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr ""
"El servidor ha finalitzat inesperadament... %1 [%2] [pàgina inicial: %3] "

#: lspclientservermanager.cpp:773
#, kde-format
msgid "Failed to find server binary: %1"
msgstr "No s'ha pogut trobar el binari del servidor: %1"

#: lspclientservermanager.cpp:776 lspclientservermanager.cpp:808
#, kde-format
msgid "Please check your PATH for the binary"
msgstr "Comproveu la vostra PATH per al binari"

#: lspclientservermanager.cpp:777 lspclientservermanager.cpp:809
#, kde-format
msgid "See also %1 for installation or details"
msgstr ""
"Vegeu també %1 per a obtindre informació sobre la instal·lació o els detalls"

#: lspclientservermanager.cpp:805
#, kde-format
msgid "Failed to start server: %1"
msgstr "No s'ha pogut iniciar el servidor: %1"

#: lspclientservermanager.cpp:813
#, kde-format
msgid "Started server %2: %1"
msgstr "S'ha iniciat el servidor %2: %1"

#: lspclientservermanager.cpp:847
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""
"No s'ha pogut analitzar la configuració del servidor «%1»: no és un objecte "
"JSON"

#: lspclientservermanager.cpp:850
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr "No s'ha pogut analitzar la configuració del servidor «%1»: %2"

#: lspclientservermanager.cpp:854
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr "No s'ha pogut llegir la configuració del servidor: %1"

#: lspclientsymbolview.cpp:238
#, kde-format
msgid "Symbol Outline"
msgstr "Contorn dels símbols"

#: lspclientsymbolview.cpp:276
#, kde-format
msgid "Tree Mode"
msgstr "Mode en arbre"

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Automatically Expand Tree"
msgstr "Expandix automàticament l'arbre"

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Sort Alphabetically"
msgstr "Ordena alfabèticament"

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Show Details"
msgstr "Mostra els detalls"

#: lspclientsymbolview.cpp:437
#, kde-format
msgid "Symbols"
msgstr "Símbols"

#: lspclientsymbolview.cpp:568
#, kde-format
msgid "No LSP server for this document."
msgstr "No hi ha cap servidor LSP per a este document."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr "Configuració del client"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:47
#, kde-format
msgid "Completions:"
msgstr "Complecions:"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:54
#, kde-format
msgid "Show inline docs for selected completion"
msgstr "Mostra els documents en línia per a la compleció seleccionada"

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Show function signature when typing a function call"
msgstr "Mostra la signatura de funció en escriure una crida de funció"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Diagnostics:"
msgstr "Diagnòstics:"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show program diagnostics"
msgstr "Mostra el diagnòstic del programa"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsHighlight)
#: lspconfigwidget.ui:97
#, kde-format
msgid "Highlight lines with diagnostics"
msgstr "Ressalta les línies amb el diagnòstic"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsMark)
#: lspconfigwidget.ui:104
#, kde-format
msgid "Show markers in the margins for lines with diagnostics"
msgstr "Mostra els marcadors en els marges per a les línies amb el diagnòstic"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsHover)
#: lspconfigwidget.ui:111
#, kde-format
msgid "Show diagnostics on hover"
msgstr "Mostra el diagnòstic en passar-hi per damunt"

#. i18n: ectx: property (toolTip), widget (QSpinBox, spinDiagnosticsSize)
#: lspconfigwidget.ui:118
#, kde-format
msgid "max diagnostics tooltip size"
msgstr "Mida màxima del consell d'eina de diagnòstic"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:133
#, kde-format
msgid "Navigation:"
msgstr "Navegació:"

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:140
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr "Compta les declaracions en buscar referències a un símbol"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:147
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr "Mostra la informació sobre el símbol actualment davall"

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:154
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr "Ressalta la línia objectiu quan s'està saltant"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:161
#, kde-format
msgid "Server:"
msgstr "Servidor:"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:168
#, kde-format
msgid "Show notifications from the LSP server"
msgstr "Mostra les notificacions des del servidor LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:175
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr "Sincronitza de manera incremental els documents amb el servidor LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:182
#, kde-format
msgid "Display additional details for symbols"
msgstr "Mostra els detalls addicionals per als símbols"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:189
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr "Presenta els símbols en una jerarquia en lloc d'una llista plana"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:204
#, kde-format
msgid "Automatically expand tree"
msgstr "Expandix automàticament l'arbre"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:213
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "Ordena alfabèticament els símbols"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:220
#, kde-format
msgid "Document outline:"
msgstr "Contorn del document:"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Enable semantic highlighting"
msgstr "Activa el ressaltat semàntic"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:241
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr "Afig les importacions automàticament si és necessari en finalitzar"

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:248
#, kde-format
msgid "Format on save"
msgstr "Format per a guardar"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:271
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr "Servidors permesos i bloquejats"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:281
#, kde-format
msgid "User Server Settings"
msgstr "Utilitza la configuració del servidor"

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:289
#, kde-format
msgid "Settings File:"
msgstr "Fitxer de configuració:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:316
#, kde-format
msgid "Default Server Settings"
msgstr "Configuració predeterminada del servidor"

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr "Més opcions"
